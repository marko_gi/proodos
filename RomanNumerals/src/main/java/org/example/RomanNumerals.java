package org.example;

public class RomanNumerals {

    public int parse (String string) throws Exception{
        if(string.equals("I"))
            return 1;
        if(string.equals("II"))
            return 2;
        if(string.equals("III"))
            return 3;
        if(string.equals("IV"))
            return 4;
        if(string.equals("V"))
            return 5;
        if(string.equals("VI"))
            return 6;
        if(string.equals("VII"))
            return 7;
        if(string.equals("VIII"))
            return 8;
        if(string.equals("IX"))
            return 9;
        if(string.equals("X"))
            return 10;
        throw new Exception("parses Strings a-i");
    }
}
