package org.example;

import org.junit.Test;

import static org.junit.Assert.*;

public class RomanNumeralsTest {

    @Test(expected = Exception.class)
    public void expectedExceptionIfTheGivenStringIsNotBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("k");
        fail("String not between I-X");
    }
    @Test
    public void expected1IfTheGivenStringIsBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("I");
        assertEquals(1,result);
    }
    @Test
    public void expected2IfTheGivenStringIsBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("II");
        assertEquals(2,result);
    }
    @Test
    public void expected3IfTheGivenStringIsBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("III");
        assertEquals(3,result);
    }
    @Test
    public void expected4IfTheGivenStringIsBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("IV");
        assertEquals(4,result);
    }
    @Test
    public void expected5fTheGivenStringIsBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("V");
        assertEquals(5,result);
    }
    @Test
    public void expected6IfTheGivenStringIsBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("VI");
        assertEquals(6,result);
    }
    @Test
    public void expected7IfTheGivenStringIsBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("VII");
        assertEquals(7,result);
    }
    @Test
    public void expected8IfTheGivenStringIsBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("VIII");
        assertEquals(8,result);
    }
    @Test
    public void expected9IfTheGivenStringIsBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("IX");
        assertEquals(9,result);
    }
    @Test
    public void expected10fTheGivenStringIsBetweenIandX() throws Exception {
        RomanNumerals romanNumerals = new RomanNumerals();
        final int result = romanNumerals.parse("X");
        assertEquals(10,result);
    }


}